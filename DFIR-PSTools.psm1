Write-Verbose "Importing DFIR PowerShell Tools"
$PS = $PSVersionTable.PSVersion.ToString()
if ($PSVersionTable.PSVersion.Major -lt 5) {
    Write-Warning "PowerShell version is not supported. Install version 5.X or higher"
    exit
}
else {
    Write-Verbose "Checking PSVersion [Minimum Supported: 5.0]: PASSED [$PS]!`n"
}

# reads in all ps1 files
. "$PSScriptRoot\scripts\utils.ps1"
. "$PSScriptRoot\scripts\asn.ps1"
. "$PSScriptRoot\scripts\convert-epochtime.ps1"
. "$PSScriptRoot\scripts\get-entropy.ps1"
. "$PSScriptRoot\scripts\file-metadata.ps1"
. "$PSScriptRoot\scripts\useragent.ps1"

Install-Module -Name DnsClient-PS -Scope CurrentUser