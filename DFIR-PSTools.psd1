# 
# Module manifest DFIR PowerShell Tools
#

@{
    RootModule = 'DFIR-PSTools.psm1'
    ModuleVersion = '1.0.0'
    GUID = '522EAD36-D5A8-421E-8907-E32222872B31'
    Author = 'Jacob Stauffer'
    CompanyName = 'JD Consultatory, LLC'
    Copyright = '© 2023 JD Consultatory, LLC. All Rights Reserved'
    PowerShellVersion = '5.1'
    FunctionsToExport = @(
        "Build-Metadata",
        "Convert-EpochTime",
        "Get-Entropy",
        "Resolve-ASN",
        "Resolve-Useragent"
    )
    Description = "Various scripts used for DFIR evidence processing"
    CmdletsToExport = @()
    VariablesToExport = '*'
    AliasesToExport = '*'
    PrivateData = @{
        PSData = @{
            Tags = @('security', 'digital forensics', 'incident response', 'blue team')
            ProjectUri = 'https://gitlab.com/jknyght9/dfir-pstools.git'
        }
    }
}