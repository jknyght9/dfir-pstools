# Digital Forensics PowerShell Tools

Recently I have adopted PowerShell as my go-to for processing large amounts of data in formats like JSON, XML, and CSV. But at times I find myself needing to resolve an IP address to an ASN or convert UNIX epoch timestamps into a usable string. Therefore I have created a module containing several scripts for processing (digital forensic) data.

## Functions

There are several functions in this module that include:

| Function          | Purpose                                                              |
|-------------------|----------------------------------------------------------------------|
| Build-Metadata    | Builds an object of metadata (size, crypto hashes) for a target file |
| Convert-EpochTime | Converts UNIX epoch time to a string                                 |
| Get-Entropy       | Calculated the entropy of a file (0..8)                              |
| Resolve-ASN       | Resolves an IP address to ASN and other important information        |
| Resolve-Useragent | Resolves a useragent to OS and browser                               |

## Installation

Clone this repository and import the module:

```powershell
Import-Module ./DFIR-Tools.psm1
```

## Getting help

Display help with the following command:

```powershell
Get-Help <COMMAND FROM ABOVE>
```

## Legal

This project is developed and maintained freely by its authors and contributors. This project may be used without restrictions; however, we ask that enhancements to this project be added on your behalf for review and mergeing. The user takes full responsibilty for using all or part of this project; the author or contributors are not responsible for unintended results from use.
