import argparse
import csv
import json 
import requests
from enum import Enum


url="https://api.whatismybrowser.com/api/v3/detect"
key="4536b08ac4b60af85deda6d71887c052"

class OutputType(Enum):
    CSV='CSV'
    JSON='JSON'
    MD='MD'

    def __str__(self):
        return self.value

def resolve_useragent(useragent):
    headers = {
        'X-API-KEY': key
    }
    post_data = {
        'headers': [
            {
                "name": "USER_AGENT",
                "value": useragent
            }
        ]
    }
    result = requests.post(url, data=json.dumps(post_data), headers=headers)
    return parse_result(useragent, result.json())

def resolve_useragentcsv(inputfile):
    with open(inputfile, newline='', encoding='utf-8-sig') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=",")
        results = []
        for row in reader:
            results.append(resolve_useragent(row['user_agent']))
    return results

def parse_result(useragent,result):
    p_result = {
        "browser": result.get('detection').get('software_name'),
        "browser_version": result.get('detection').get('software_version'),
        "operating_system": result.get('detection').get('operating_system'),
        "simple_string": result.get('detection').get('simple_software_string'),
        "useragent": useragent
    }
    return p_result

def output_csv(result,outputfile):
    if (outputfile):
        outfile = open(outputfile, 'w')
    else:
        outfile = open('resolve-useragent-output.csv', 'w')
    csv_writer = csv.writer(outfile)
    count = 0
    for r in result:
        if count == 0:
            header = r.keys()
            csv_writer.writerow(header)
            count += 1
        csv_writer.writerow(r.values())
    outfile.close()

def output_markdown(result,outputfile):
    if (outputfile):
        outfile = open(outputfile, 'w')
    else:
        outfile = open('resolve-useragent-output.md', 'w')
    count = 0
    mdstring = ''
    for r in result:
        if count == 0:
            mdstring = " | ".join(list(r.keys()))
            mdstring += "\n---|---|---|---|---\n"
            count += 1
        mdstring += " | ".join(list(r.values())) + "\n"
    outfile.write(mdstring)
    outfile.close()

def output_json(result,outputfile):
    if (outputfile):
        outfile = open(outputfile, 'w')
    else:
        outfile = open('resolve-useragent-output.json', 'w')
    outfile.write(json.dumps(result, indent=2))
    outfile.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="resolve-useragent.py",
        description="Web browser user agent resolver"
    )
    parser.add_argument('-ua', dest='useragent', action='store', help="User Agent as a string with quotes")
    parser.add_argument('-uacsv', dest='useragentcsv', action='store', help="User Agent CSV file with 'user_agent' header")
    parser.add_argument('-output', dest='output', action='store', type=OutputType, choices=list(OutputType))
    parser.add_argument('-outputfile', dest='outputfile', action='store', help='File to output result')
    args = parser.parse_args()
    if (args.useragent or args.useragentcsv):
        if (args.useragent):
            result = [resolve_useragent(args.useragent)]
        if (args.useragentcsv):
            result = resolve_useragentcsv(args.useragentcsv)
        if (args.output and result):
            if (args.output is OutputType.CSV):
                output_csv(result, args.outputfile)
            if (args.output is OutputType.MD):
                output_markdown(result, args.outputfile)
            if (args.output is OutputType.JSON):
                output_json(result, args.outputfile)
        else:
            for r in result:
                print(r.get("simple_string"))
    else:
        parser.print_help()