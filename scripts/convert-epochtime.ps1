<#
.SYNOPSIS
    Converts UNIX timestamps (epoch time) to a human readable timestamp
.PARAMETER EpochTime
    Specifies an integer to convert to a date time stamp
.EXAMPLE
    C:\PS>Convert-EpochTime -EpochTime 1687883772
.EXAMPLE
    C:\PS>1687883838 | Convert-EpochTime
.OUTPUTS
    System.DateTime
    Outputs a date time stamp Universal sortable data/time pattern "yyyy-MM-dd HH:mm:ss"
.NOTES
    Version:        1.0
    Author:         Jacob Stauffer
#>
function Convert-EpochTime {
    param (
        [Parameter(Mandatory=$True, Position=0, ValueFromPipeline=$True, ParameterSetName='EpochTime')]
        [ValidateNotNullOrEmpty()]
        [int]$EpochTime
    )

    process {
        $digits = $EpochTime.ToString().Length
        if ($digits -eq 10) {
            $dt = [System.DateTimeOffset]::FromUnixTimeSeconds($EpochTime).DateTime
        }
        if ($digits -eq 13) {
            $dt = [System.DateTimeOffset]::FromUnixTimeMilliseconds($EpochTime).DateTime
        }
        if ($digits -eq 16) {
            $dt = [System.DateTimeOffset]::FromUnixTimeMilliseconds($EpochTime/1000).DateTime
        }
        if ($digits -eq 19) {
            $dt = [System.DateTimeOffset]::FromUnixTimeMilliseconds($EpochTime/1000000).DateTime
        }
    }

    end {
        if ($dt) {
            Get-Date -Format "u" $dt
        }
        else {
            Write-Host "Could not convert $EpochTime"
        }
    }
}