function ConvertTo-MarkdownColumn {
    [CmdletBinding()]
    [OutputType([string])]
    param (
        [Parameter(Mandatory)]
        $Collection
    )

    begin {
        $col1size = 0
        $col2size = 0
        foreach ($item in $Collection) {
            $item.PSObject.Properties | ForEach-Object {
                if ($_.Name.ToString().Length -gt $col1size) { $col1size = $_.Name.ToString().Length}
                if ($_.Value.ToString().Length -gt $col2size) { $col2size = $_.Value.ToString().Length}
            }
        }
    }

    process {
        foreach ($item in $Collection) {
            @("".PadRight($col1size, " "), "".PadRight($col2size, " ")) -join " | "
            @("".PadRight($col1size, "-"), "".PadRight($col2size, "-")) -join " | "
            $item.PSObject.Properties | ForEach-Object { @($_.Name.PadRight($col1size, " "),$_.Value.PadRight($col2size, " ")) -join " | " }
            "`n---`n"
        }
    }
}

function ConvertTo-MarkdownRow {
    [CmdletBinding()]
    [OutputType([string])]
    param (
        [Parameter(Mandatory)]
        $Collection
    )

    begin {
        $columns = [ordered]@{}
        foreach ($item in $Collection) {
            $item.PSObject.Properties | ForEach-Object {
                if ($columns[$_.Name] -lt $_.Value.ToString().Length) {
                    $columns[$_.Name] = $_.Value.ToString().Length
                }
            }
        }
        $columns
    }

    process {
        ($columns.GetEnumerator() | ForEach-Object {
            $_.Name.PadRight($_.Value, " ")
        }) -join " | "
        ($columns.GetEnumerator() | ForEach-Object {
            "".PadRight($_.Value, "-")
        }) -join " | "
        foreach ($item in $Collection) {
            ($item.PSObject.Properties | ForEach-Object {
                $_.Value.PadRight($columns[$_.Name]," ")
            }) -join " | "
        }
    }
}