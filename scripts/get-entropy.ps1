<#
.SYNOPSIS
    Updated version of Matthew Graeber's PowerSploit Function. Now calculates entropy of files larger than 2GB.
.PARAMETER ByteArray
    Specifies the byte array containing the data from which entropy will be calculated.
.PARAMETER FilePath
    Specifies the path to the input file from which entropy will be calculated.
.EXAMPLE
    C:\PS>Get-Entropy -FilePath C:\Windows\System32\kernel32.dll
.EXAMPLE
    C:\PS>ls C:\Windows\System32\*.dll | % { Get-Entropy -FilePath $_ }
.EXAMPLE
    C:\PS>$RandArray = New-Object Byte[](10000)
    C:\PS>foreach ($Offset in 0..9999) { $RandArray[$Offset] = [Byte] (Get-Random -Min 0 -Max 256) }
    C:\PS>$RandArray | Get-Entropy

    Description
    -----------
    Calculates the entropy of a large array containing random bytes.
.EXAMPLE
    C:\PS> 0..255 | Get-Entropy

    Description
    -----------
    Calculates the entropy of 0-255. This should equal exactly 8.
.OUTPUTS
    System.Double
    Get-Entropy outputs a double representing the entropy of the byte array.
.NOTES
    Version:        1.0
    Author:         Matthew Graeber (original)
    Contributor:    Jacob Stauffer
#>
function Get-Entropy
{
    [CmdletBinding()] Param (
        [Parameter(Mandatory = $True, Position = 0, ValueFromPipeline = $True, ParameterSetName = 'Bytes')]
        [ValidateNotNullOrEmpty()]
        [Byte[]]
        $ByteArray,

        [Parameter(Mandatory = $True, Position = 0, ParameterSetName = 'File')]
        [ValidateNotNullOrEmpty()]
        [String]
        $FilePath
    )

    BEGIN
    {
        $FrequencyTable = @{}
        $ByteArrayLength = 0
    }

    PROCESS
    {
        if ($PsCmdlet.ParameterSetName -eq 'File')
        {
            try {
                $InFile = [IO.File]::OpenRead((Get-Item $FilePath).FullName)
                $FileLength = ([IO.FileInfo] $FilePath).Length
                $Finished = $False
                $Window = 512MB

                if ($FileLength -ge $Window) {
                    $Bytes = New-Object byte[] $Window
                }
                else {
                    $Bytes = New-Object byte[] $FileLength
                }

                while(!$Finished) {
                    $BytesToRead = $FileLength
                    while ($BytesToRead) {
                        $BytesRead = $InFile.Read($Bytes, 0, [Math]::Min([Double]$Bytes.Length, [Double]$BytesToRead))

                        foreach ($Byte in $Bytes) {
                            $FrequencyTable[$Byte]++
                            $ByteArrayLength++
                        }

                        if (!$BytesRead) {
                            $Finished = $True
                            break
                        }

                        $BytesToRead -= $BytesRead
                    }
                }
            }
            catch { Write-Error $_ }
            finally {
                $InFile.Close()
                $InFile.Dispose()
            }
        }
        else {
            foreach ($Byte in $ByteArray) {
                $FrequencyTable[$Byte]++
                $ByteArrayLength++
            }
        }
    }

    END
    {
        $Entropy = 0.0

        foreach ($Byte in 0..255)
        {
            $ByteProbability = ([Double] $FrequencyTable[[Byte]$Byte]) / $ByteArrayLength
            if ($ByteProbability -gt 0)
            {
                $Entropy += -$ByteProbability * [Math]::Log($ByteProbability, 2)
            }
        }

        Write-Output $Entropy
    }
}