<#
.SYNOPSIS
    Builds a catalog of metadata for a file, list of files, or all files within a directory.
.PARAMETER Filename
    Specifies a single or list of files to build metadata from.
.PARAMETER Directory
    Specifies a directory of files to build metadata from.
.PARAMETER CSV
    Specifies output results to CSV file.
.PARAMETER HTML
    Specifies output results to HTML file.
.PARAMETER JSON
    Specifies output results to JSON file.
.PARAMETER MD
    Specifies output results to markdown file.
.PARAMETER OutFile
    Specifies output results to a file with a specific name. Used with CSV | MD | JSON parameters.
.EXAMPLE
    C:\PS>Build-Metadata -Filename test.exe
.EXAMPLE
    C:\PS>Build-Metadata -Filename (Get-ChildItem -Path ../ -Filter "*.png").FullName)
.EXAMPLE
    C:\PS>Build-Metadata -Filename test.exe -CSV -JSON -OutFile metadata-catalog
.EXAMPLE
    C:\PS>Resolve-Useragent -Directory ./target-directory -CSV -JSON -OutFile metadata-catalog
.OUTPUTS
    Console.Write
    Outputs to the console if optional formating parameters are not set

    File
    Outputs to specified file
.NOTES
    Version:        1.0
    Author:         Jacob Stauffer
#>

function Build-Metadata {
    param(
        [Parameter(Mandatory,ParameterSetName="File")]
        [IO.FileInfo[]]$Filename,
        [Parameter(Mandatory,ParameterSetName="Directory")]
        [string]$Directory,
        [Parameter()]
        [switch]$CSV,
        [Parameter()]
        [switch]$HTML,
        [Parameter()]
        [switch]$JSON,
        [Parameter()]
        [switch]$MD,
        [Parameter()]
        [String]$OutFile
    )

    begin {
        if ($Directory) {
            $files = Get-ChildItem -Path $Directory | ForEach-Object { (Join-Path $Directory $_.Name) }
        }
        else { $files = @($Filename) }
    }

    process {
        $metadatas = @()
        foreach ($file in $files) {
            if (Test-Path -Path $file -PathType Leaf) {
                $metadatas += Get-FileMetadata -Filename $file
            }
        }
        if ($metadatas) {
            if (!($OutFile)) {
                $OutFile = (Get-Date -Format "yyyyMMddTHHmmss" -AsUTC) + "-metadata"
            }
            if ($CSV) { $metadatas | ConvertTo-Csv | Out-File -FilePath "$OutFile.csv" -NoClobber -Encoding utf8 }
            if ($HTML) { $metadatas | ConvertTo-Html | Out-File -FilePath "$OutFile.html" -NoClobber -Encoding utf8}
            if ($JSON) { $metadatas | ConvertTo-Json | Out-File -FilePath "$OutFile.json" -NoClobber -Encoding utf8}
            if ($MD) { ConvertTo-MarkdownColumn -Collection $metadatas | Out-File -FilePath "$OutFile.md" -NoClobber -Encoding utf8 }
        }
        else {
            Write-Error "Could not build file metadata"
        }
    }

    end {
        if ($CSV -Or $HTML -Or $JSON -Or $MD -Or $XML) {
            Write-Host "Results outputed to $Outfile.csv | .html | .json | .md | .xml"
        }
        else { $metadatas }
    }
}

function Get-FileMetadata {
    param(
        [Parameter(Mandatory)]
        $Filename
    )

    begin {
        $bins = Test-Bins
    }

    process {
        if (Test-Path -Path $Filename) {
            $metadata = New-Object -TypeName psobject -Property ([ordered]@{})
            $metadata | Add-Member -Type NoteProperty -Name Filename -Value (Split-Path $Filename -Leaf)
            if ($bins.filebin) {
                $metadata | Add-Member -Type NoteProperty -Name Type -Value (Get-ProcessOutput -Filename $bins.filebin -ArgsList $Filename).split(":")[1].trim()
            }
            $metadata | Add-Member -Type NoteProperty -Name Size -Value ((Get-Item $Filename).Length.ToString() + " bytes")
            $metadata | Add-Member -Type NoteProperty -Name MD5 -Value (Get-FileHash -Algorithm MD5 $Filename).Hash
            $metadata | Add-Member -Type NoteProperty -Name SHA1 -Value (Get-FileHash -Algorithm SHA1 $Filename).Hash
            $metadata | Add-Member -Type NoteProperty -Name SHA256 -Value (Get-FileHash -Algorithm SHA256 $Filename).Hash
            if ($bins.ssdeepbin) {
                $metadata | Add-Member -Type NoteProperty -Name SSDeep -Value (Get-ProcessOutput -Filename $bins.ssdeepbin -ArgsList $Filename).split("`r`n")[1].split(",")[0].trim()
            }
            $metadata | Add-Member -Type NoteProperty -Name Entropy -Value (Get-Entropy -FilePath $Filename)
        }
    }

    end {
        return $metadata
    }
}

function Get-ProcessOutput {
    Param (
        [Parameter(Mandatory=$true)]
        $FileName,
        $ArgsList
    )
    $fileoutput = (New-Guid).Guid
    Start-Process -FilePath $FileName -ArgumentList """$ArgsList""" -NoNewWindow -PassThru -Wait -RedirectStandardOutput $fileoutput -ErrorAction SilentlyContinue | Out-Null
    $filetype = Get-Content $fileoutput
    Remove-Item -Path $fileoutput
    return $filetype
}

function Test-Bins {
    process {
        if ($IsMacOS -Or $IsLinux) {
            $bins = New-Object -TypeName psobject -Property @{
                filebin = (which file) | Out-String
            }
            $ssdeepbin = (which ssdeep) | Out-String
            if ($ssdeepbin) {
                $bins | Add-Member -Type NoteProperty -Name ssdeepbin -Value $ssdeepbin
            }
            else { Write-Host "SSDeep binary not installed" }
        }
        else {
            Write-Warning "This operating system does not have the required binaries to perform file type lookups and/or SSDEEP hashing"
        }
    }
    end {
        return $bins
    }
}