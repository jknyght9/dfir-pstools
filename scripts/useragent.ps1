<#
.SYNOPSIS
    Resolves a user-agent to approximate operating system, version, and browser
.PARAMETER UserAgent
    Specifies a single user agent string to resolve
.PARAMETER UserAgentCSV
    Specifies a CSV of user agent strings to resolve. Must be a single column with header "useragent"
.PARAMETER CSV
    Specifies output results to CSV file
.PARAMETER JSON
    Specifies output results to JSON file
.PARAMETER MD
    Specifies output results to markdown file
.PARAMETER OutFile
    Specifies output results to a file with a specific name. Used with CSV | MD | JSON parameters
.EXAMPLE
    C:\PS>Resolve-Useragent -UserAgent "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"
.EXAMPLE
    C:\PS>Resolve-Useragent -UserAgent "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0" -JSON -OutFile resolved_useragent
.EXAMPLE
    C:\PS>Resolve-Useragent -UserAgentCSV test.csv -CSV -JSON -OutFile resolved_useragents
.OUTPUTS
    Console.Write
    Outputs to the console if optional formating parameters are not set

    File
    Outputs to specified file
.NOTES
    Version:        1.0
    Author:         Jacob Stauffer
#>
function Resolve-Useragent {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$False, ValueFromPipeline=$True, ParameterSetName='UserAgent')]
        [String]$UserAgent,
        [Parameter(Mandatory=$False, ValueFromPipeline=$False, ParameterSetName='UserAgentCSV')]
        [String]$UserAgentCSV,
        [Parameter(Mandatory=$False)]
        [switch]$CSV,
        [Parameter(Mandatory=$False)]
        [switch]$JSON,
        [Parameter(Mandatory=$False)]
        [switch]$MD,
        [Parameter(Mandatory=$False)]
        [String]$OutFile
    )

    process {
        if ($UserAgentCSV) {
            $result = Test-Useragents -CSVFile $UserAgentCSV
        }
        else {
            $result = Test-Useragent -UserAgent $UserAgent
        }
        if ($result) {
            if (!($OutFile)) {
                $OutFile = (Get-Date -Format "yyyyMMddTHHmmss" -AsUTC) + "-metadata"
            }
            if ($CSV) { $result | ConvertTo-Csv | Out-File -FilePath "$OutFile.csv" -NoClobber -Encoding utf8 }
            if ($JSON) { $result | ConvertTo-Json | Out-File -FilePath "$OutFile.json" -NoClobber -Encoding utf8 }
            if ($MD) { ConvertTo-MarkdownRow -Collection $result | Out-File -FilePath "$OutFile.md" -NoClobber -Encoding utf8 }
        }
        else {
            Write-Error "Could not resolve useragent"
        }
    }

    end {
        if ($CSV -Or $JSON -Or $MD ) {
            Write-Host "Results outputed to $Outfile.csv | .json | .md "
        }
        else { $result.simple_string }
    }
}

function Test-Useragent {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True, Position = 0, ParameterSetName = 'UserAgent')]
        [ValidateNotNullOrEmpty()]
        [String]$UserAgent
    )

    begin {
        $api_url="https://api.whatismybrowser.com/api/v3/detect"
        $api_key="4536b08ac4b60af85deda6d71887c052"
    }

    process {
        $request_params = @{
            Uri = $api_url
            Method = 'POST'
            Headers =  @{'X-API-KEY' = $api_key}
            Body = (@{"headers" = @(@{ "name" = "USER_AGENT"; "value" = $UserAgent })} | ConvertTo-Json)
            ContentType = "application/json; charset=utf-8"
        }
        $result = ((Invoke-WebRequest @request_params).Content | ConvertFrom-Json)
    }

    end {
        return New-Object psobject -Property @{
            "browser" = $result.detection.software_name
            "browser_version" = $result.detection.software_version
            "operating_system" = $result.detection.operating_system
            "simple_string" = $result.detection.simple_software_string
            "useragent" = $UserAgent
        }
    }
}

function Test-Useragents {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True, Position = 0, ParameterSetName = 'UserAgents')]
        [ValidateNotNullOrEmpty()]
        [String]$CSVFile
    )

    process {
        $useragents = Import-Csv -Path $CSVFile
        $results = @()
        foreach ($ua in $useragents) {
            $results += ((Test-Useragent -UserAgent $ua.useragent))
        }
    }

    end {
        return $results
    }
}