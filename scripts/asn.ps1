<#
.SYNOPSIS
    Resolves an IPv4 address's Autonomous System Number (ASN) and other key information.
.PARAMETER IPAddress
    Specifies an IPv4 address to resolve
.PARAMETER IPAddressCSV
    Specifies a CSV of IPv4 address strings to resolve. Must be a single column with header "ipaddress"
.PARAMETER CSV
    Specifies output results to CSV file
.PARAMETER JSON
    Specifies output results to JSON file
.PARAMETER MD
    Specifies output results to markdown file
.PARAMETER OutFile
    Specifies output results to a file with a specific name. Used with CSV | MD | JSON parameters
.EXAMPLE
    C:\PS>Resolve-ASN -IPAddress 10.1.67.254
.EXAMPLE
    C:\PS>Resolve-ASN -IPAddress 10.1.67.254 -JSON -OutFile resolved_ASN
.EXAMPLE
    C:\PS>Resolve-ASN -IPAddressCSV ips.csv -CSV -JSON -OutFile resolved_ASN
.OUTPUTS
    Console.Write
    Outputs to the console if optional formating parameters are not set

    File
    Outputs to specified file
.NOTES
    Version:        1.0
    Author:         Jacob Stauffer
#>

function Resolve-ASN {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$False,ParameterSetName="IPAddress")]
        [String]$IPAddress,
        [Parameter(Mandatory=$False,ParameterSetName="IPAddressCSV")]
        [String]$IPAddressCSV,
        [Parameter(Mandatory=$False)]
        [switch]$CSV,
        [Parameter(Mandatory=$False)]
        [switch]$JSON,
        [Parameter(Mandatory=$False)]
        [switch]$MD,
        [Parameter(Mandatory=$False)]
        [String]$OutFile
    )

    process {
        if ($IPAddressCSV) {
            $result = Test-ASNs -CSVFile $IPAddressCSV
        }
        else {
            $result = Test-ASN -IPAddress $IPAddress
        }
        if ($result) {
            if (!($OutFile)) {
                $OutFile = (Get-Date -Format "yyyyMMddTHHmmss" -AsUTC) + "-metadata"
            }
            if ($CSV) { $result | ConvertTo-Csv | Out-File -FilePath "$OutFile.csv" -NoClobber -Encoding utf8 }
            if ($JSON) { $result | ConvertTo-Json | Out-File -FilePath "$OutFile.json" -NoClobber -Encoding utf8}
            if ($MD) { ConvertTo-MarkdownRow -Collection $result | Out-File -FilePath "$OutFile.md" -NoClobber -Encoding utf8 }
        }
        else {
            Write-Error "Could not resolve ASN information"
        }
    }

    end {
        if ($CSV -Or $JSON -Or $MD ) {
            Write-Host "Results outputed to $Outfile.csv | .json | .md "
        }
        else { $result }
    }
}

function Test-ASN {
    param (
        [Parameter(Mandatory=$True)]
        [ValidatePattern("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")]
        [ValidateScript( {
        #verify each octet is valid to simplify the regex
            $test = ($_.split(".")).where({[int]$_ -gt 254})
            if ($test) {
                Throw "$_ does not appear to be a valid IPv4 address"
                $false
            }
            else {
                $true
            }
        })]
        [string]$IPAddress
    )
    
    begin {
        $asn_url = ".asn.cymru.com"
        $origin_url = ".origin$asn_url"
    }

    process {
        $octets = $IPAddress.Split(".")
        $reversed = $octets[3] + "." + $octets[2] + "." + $octets[1] + "." + $octets[0]
        $origin_query = (Resolve-Dns -QueryType TXT -Query $reversed$origin_url).Answers

        if ($origin_query) {
            $origin_result = New-Object psobject -Property @{
                "IPAddress" = $IPAddress
                "ASNumber" = $origin_query.Text.Split('|')[0].Trim()
                "BGP Prefix" = $origin_query.Text.Split('|')[1].Trim()
                "Country" = $origin_query.Text.Split('|')[2].Trim()
                "Registry" = $origin_query.Text.Split('|')[3].Trim()
                "Allocated" = $origin_query.Text.Split('|')[4].Trim()
            }
            $as_url = "as" + $origin_result.ASNumber + $asn_url
            $asn_query = (Resolve-Dns -QueryType TXT -Query $as_url).Answers
            $origin_result | Add-Member -Type NoteProperty -Name "AS Name" -Value $asn_query.Text.Split('|')[4].Trim()
            return $origin_result
        }
    }
}

function Test-ASNs {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]$CSVFile
    )

    process {
        $ips = Import-Csv -Path $CSVFile
        $results = @()
        foreach ($ip in $ips) {
            $results += ((Test-ASN -IPAddress $ip.ipaddress))
        }
    }

    end {
        return $results
    }
}